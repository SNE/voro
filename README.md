# Voro
[![pipeline status](https://gitlab.lrz.de/SNE/voro/badges/master/pipeline.svg)](https://gitlab.lrz.de/SNE/voro/-/commits/master)

This project is a fork of the Voro++ project you can find here
[http://math.lbl.gov/voro++](http://math.lbl.gov/voro++).\
It only contains the library part of the original project. If you are interested
in the command-line utility or additional scripts and examples, please visit the
homepage of the original project.

The aim of this project is:
1. to provide a cmake build framework for the voro++ library, so it can be easily
   integrated in other projects as a submodule.
2. to provide a python module wrapping (parts of) the voro++ library and make its
   functionality accessible in python scripts.


## Usage
The Voro++ library provides a bunch of C++ classes and routines to perform
Voronoi tesellations in 3D. Use the [API reference](http://math.lbl.gov/voro++/doc/refman/)
of the original project for details.

We take no credit in the development of the library. If you use this project in
academic research, consider citing the publications of the original author.
A list of publications can be found [here](http://math.lbl.gov/voro++/download/).

### Build
First you need to clone this git repository. For the python bindings we use
[pybind11](https://github.com/pybind/pybind11), which is included as a git submodule.
You need to initialize and update the submodule in order to build the python module.
```shell
# clone repository
git clone https://gitlab.lrz.de/SNE/voro.git
# update dependencies
git submodule update --init
```
If you want to build the Voro++ library, just follow the standard cmake procedure.\
This will give you the shared library file `libvoro++.so`.
```shell
mkdir -p build
cd build
cmake ..
make
```
If you want to build the python module add the cmake option `BUILD_PYVORO=ON`.\
This will give you the file `pyvoro.<version_info>.so`.
```shell
cmake -D BUILD_PYVORO=ON ..
make
```
It is also possible to build the API refernce yourself. For this you need *doxygen*
and *graphviz* installed on your system. This will generate the folder `doc/html`
containing the documentation. Open the `index.html` file with your favourite browser.
```shell
cmake -D BUILD_DOC=ON ..
make voro_doc
```

### Use as a submodule
If you want to include the Voro++ library in your git project, a good way is to
use git submodules.
```shell
git submodule add https://gitlab.lrz.de/SNE/voro.git path/to/voro
git submodule update --init --recursive
```
In your projects main `CMakeLists.txt` file you add the submodule with the
`add_subdirectory()` command and then you can link your targets with the
`target_link_libraries()` command.
If you would like to build the python module by default, you can set the option
before you add the subdirectory.
```cmake
set(BUILD_PYVORO ON CACHE BOOL "Enable pyvoro build.")
add_subdirectory(path/to/voro)

...

# link your target to the Voro++ library
target_link_libraries(your_target PRIVATE libvoro++)
```


## License
This project is released under the [MIT License](LICENSE).
