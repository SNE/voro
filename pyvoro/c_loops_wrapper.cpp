/**
 * pyvoro, a python module wrapping the Voro++ library.
 *
 * Copyright (c) 2020, Michael Rinderle  <michael.rinderle@tum.de>
 *
 * This is free software, released under the MIT license.
 * Further details can be found in the LICENSE file.
 */

#include <pybind11/pybind11.h>

#include "voro++.hh"

namespace py = pybind11;

void c_loops_wrapper(py::module& m) {
    py::class_<voro::c_loop_all>(m, "c_loop_all")
        // constructor
        .def(py::init<voro::container&>(), py::arg("con"))
        // methods
        .def("start", &voro::c_loop_all::start)
        .def("inc", &voro::c_loop_all::inc)
        // inherited methods
        .def("pid", &voro::c_loop_all::pid);

    py::class_<voro::c_loop_all_periodic>(m, "c_loop_all_periodic")
        // constructor
        .def(py::init<voro::container_periodic&>(), py::arg("con"))
        // methods
        .def("start", &voro::c_loop_all_periodic::start)
        .def("inc", &voro::c_loop_all_periodic::inc)
        // inherited methods
        .def("pid", &voro::c_loop_all_periodic::pid);
}
