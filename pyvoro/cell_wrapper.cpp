/**
 * pyvoro, a python module wrapping the Voro++ library.
 *
 * Copyright (c) 2020, Michael Rinderle  <michael.rinderle@tum.de>
 *
 * This is free software, released under the MIT license.
 * Further details can be found in the LICENSE file.
 */

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "voro++.hh"

namespace py = pybind11;

// make std vectors opaque
PYBIND11_MAKE_OPAQUE(std::vector<int>);
PYBIND11_MAKE_OPAQUE(std::vector<double>);

void cell_wrapper(py::module& m) {
    py::class_<voro::voronoicell_neighbor>(m, "voronoicell_neighbor")
        // constructor
        .def(py::init<>())
        // methods
        .def("neighbors", &voro::voronoicell_neighbor::neighbors, py::arg("v"))
        .def("face_areas", &voro::voronoicell_neighbor::face_areas, py::arg("v"))
        .def("volume", &voro::voronoicell_neighbor::volume)
        .def("vertices",
             (void (voro::voronoicell_neighbor::*)(std::vector<double>&)) & voro::voronoicell_neighbor::vertices,
             py::arg("v"))
        .def("vertex_orders", &voro::voronoicell_neighbor::vertex_orders, py::arg("v"))
        .def("face_vertices", &voro::voronoicell_neighbor::face_vertices, py::arg("v"))
        .def("face_orders", &voro::voronoicell_neighbor::face_orders, py::arg("v"))
        .def("normals", &voro::voronoicell_neighbor::normals, py::arg("v"));
}
