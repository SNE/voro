/**
 * pyvoro, a python module wrapping the Voro++ library.
 *
 * Copyright (c) 2020, Michael Rinderle  <michael.rinderle@tum.de>
 *
 * This is free software, released under the MIT license.
 * Further details can be found in the LICENSE file.
 */

#include <pybind11/pybind11.h>

#include "voro++.hh"

namespace py = pybind11;

void container_prd_wrapper(py::module& m) {
    py::class_<voro::container_periodic>(m, "container_periodic")
        // constructor
        .def(py::init<double, double, double, double, double, double, int, int, int, int>(), py::arg("bx"),
             py::arg("bxy"), py::arg("by"), py::arg("bxz"), py::arg("byz"), py::arg("bz"), py::arg("nx"), py::arg("ny"),
             py::arg("nz"), py::arg("init_mem"))
        // methods
        .def("put", (void (voro::container_periodic::*)(int, double, double, double)) & voro::container_periodic::put,
             py::arg("n"), py::arg("x"), py::arg("y"), py::arg("z"))
        .def("compute_cell",
             &voro::container_periodic::compute_cell<voro::voronoicell_neighbor&, voro::c_loop_all_periodic&>,
             py::arg("v_cell"), py::arg("c_loop"));
}
