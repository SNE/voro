/**
 * pyvoro, a python module wrapping the Voro++ library.
 *
 * Copyright (c) 2020, Michael Rinderle  <michael.rinderle@tum.de>
 *
 * This is free software, released under the MIT license.
 * Further details can be found in the LICENSE file.
 */

#include <pybind11/pybind11.h>

#include "voro++.hh"

namespace py = pybind11;

void container_wrapper(py::module& m) {
    py::class_<voro::container>(m, "container")
        // constructor
        .def(py::init<double, double, double, double, double, double, int, int, int, bool, bool, bool, int>(),
             py::arg("ax"), py::arg("bx"), py::arg("ay"), py::arg("by"), py::arg("az"), py::arg("bz"), py::arg("nx"),
             py::arg("ny"), py::arg("nz"), py::arg("xperiodic"), py::arg("yperiodic"), py::arg("zperiodic"),
             py::arg("init_mem"))
        // methods
        .def("put", (void (voro::container::*)(int, double, double, double)) & voro::container::put, py::arg("n"),
             py::arg("x"), py::arg("y"), py::arg("z"))
        .def("compute_cell", &voro::container::compute_cell<voro::voronoicell_neighbor&, voro::c_loop_all&>,
             py::arg("v_cell"), py::arg("c_loop"));
}

// template functions cannot be exported directly
// each instanciated version has to be exported separately
