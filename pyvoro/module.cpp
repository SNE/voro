/**
 * pyvoro, a python module wrapping the Voro++ library.
 *
 * Copyright (c) 2020, Michael Rinderle  <michael.rinderle@tum.de>
 *
 * This is free software, released under the MIT license.
 * Further details can be found in the LICENSE file.
 */

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

// forward declarations of class wrappers
void c_loops_wrapper(py::module& m);
void cell_wrapper(py::module& m);
void container_prd_wrapper(py::module& m);
void container_wrapper(py::module& m);

// make std vectors opaque
PYBIND11_MAKE_OPAQUE(std::vector<int>);
PYBIND11_MAKE_OPAQUE(std::vector<double>);

// python module
PYBIND11_MODULE(pyvoro, m) {
    // module docstring
    m.doc() = "Python bindings for the Voro++ library performing 3-D voronoi tesselation.";

    c_loops_wrapper(m);
    cell_wrapper(m);
    container_prd_wrapper(m);
    container_wrapper(m);

    // wrapper for std::vector of integers
    py::class_<std::vector<int>>(m, "IntVector")
        .def(py::init<>())
        .def("clear", &std::vector<int>::clear)
        .def("pop_back", &std::vector<int>::pop_back)
        .def("__len__", [](const std::vector<int>& v) { return v.size(); })
        .def(
            "__iter__", [](std::vector<int>& v) { return py::make_iterator(v.begin(), v.end()); },
            py::keep_alive<0, 1>());
    // wrapper for std::vector of doubles
    py::class_<std::vector<double>>(m, "DoubleVector")
        .def(py::init<>())
        .def("clear", &std::vector<double>::clear)
        .def("pop_back", &std::vector<double>::pop_back)
        .def("__len__", [](const std::vector<double>& v) { return v.size(); })
        .def(
            "__iter__", [](std::vector<double>& v) { return py::make_iterator(v.begin(), v.end()); },
            py::keep_alive<0, 1>());
}
